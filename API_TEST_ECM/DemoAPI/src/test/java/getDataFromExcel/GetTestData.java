package getDataFromExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import driverPack.DriverClass;

public class GetTestData {

	static String getColValue;
	//Get this data  from properties file and update here
	static String testDataFileName = "ECM_TestData.xlsx";
	static String pattern;

	public static String GetRelativeTestData(String ColumnName) throws IOException {
		String ProjectPath = System.getProperty("user.dir");
		String TestDataFilePath = ProjectPath+File.separator+"TestData\\"+testDataFileName;
		FileInputStream TestdataFilePath = new FileInputStream (TestDataFilePath);

		XSSFWorkbook workbook = new XSSFWorkbook(TestdataFilePath);   
		XSSFSheet sheet = workbook.getSheet(DriverClass.ScenarioName);    

		XSSFRow row = sheet.getRow(1);
		int colCount = row.getLastCellNum();
		int rowCount = sheet.getLastRowNum();

		for(int l=2;l<=rowCount;l++){

			Row getTCIDSheet=sheet.getRow(l); //returns the logical row  
			Cell getTCIDCell=getTCIDSheet.getCell(0); //getting the cell representing the given column  
			Cell DataID = getTCIDSheet.getCell(1);
			String getTCID=getTCIDCell.getStringCellValue(); 
			String TestData_ID = DataID.getStringCellValue();

			//System.out.println("TCID: "+DriverClass.TCID);
			//System.out.println("TestDataID: "+DriverClass.TestDataID);
			
			if (getTCID.equalsIgnoreCase(DriverClass.TCID) && TestData_ID.equalsIgnoreCase(DriverClass.TestDataID) && DriverClass.TestDataID != "" ) {
				//System.out.println("Entered Test Data");
				for (int m=2;m<=colCount;m++) {
					Row TcID=sheet.getRow(1); 
					Cell dataColumn=TcID.getCell(m);  
					String ColumnHeader = dataColumn.getStringCellValue();
						//System.out.println("ColumnHeader: "+ColumnHeader);
					if (ColumnHeader.equalsIgnoreCase(ColumnName) ) {
						//System.out.println("Entered Test Data column");
						Row TctestCaseID=sheet.getRow(l); 
						Cell getdataColumn=TctestCaseID.getCell(m);
						String testData = getdataColumn.getStringCellValue();
						getColValue = testData;
						break;
					}

				}

			}

			workbook.close();
		}		
		return getColValue;	
	}


	public static String getReplacePattern(String PatternType) throws IOException {
		String ProjectPath = System.getProperty("user.dir");
		String TestDataFilePath = ProjectPath+File.separator+"TestData\\"+testDataFileName;
		FileInputStream TestdataFilePath = new FileInputStream (TestDataFilePath);

		XSSFWorkbook workbook = new XSSFWorkbook(TestdataFilePath);   
		XSSFSheet sheet = workbook.getSheet("ReplacePattern");    

		XSSFRow row = sheet.getRow(1);
		int colCount = row.getLastCellNum();
		//int rowCount = sheet.getLastRowNum();
		//System.out.println("Column Count "+colCount);
		for (int n=0;n<=colCount;n++) {
			Row TcID=sheet.getRow(0); 
			Cell dataColumn=TcID.getCell(n);  
			String ColumnHeader = dataColumn.getStringCellValue();
			//System.out.println("column header "+ColumnHeader);
			if (ColumnHeader.equalsIgnoreCase(PatternType) ) {

				Row TctestCaseID=sheet.getRow(1); 
				Cell getdataColumn=TctestCaseID.getCell(n);
				String testData = getdataColumn.getStringCellValue();
				pattern = testData;
				break;
			}
			
			//System.out.println("pattern is "+pattern);
		}
		workbook.close();
		return pattern;
	}


}  