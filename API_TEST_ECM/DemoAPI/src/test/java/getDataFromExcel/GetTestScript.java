package getDataFromExcel;

import java.io.IOException;

import org.testng.annotations.Test;

import DemoTestAssured.DemoAPI.EcmAPImethods;
import DemoTestAssured.DemoAPI.OtherSupportingMethod;
import databaseConnection.SiviewDBConnect;


@Test
public class GetTestScript {

	static String TestScriptStatus;
	static String replacePattern;

	public static String ExecuteMethods(String Method) throws Exception, IOException {

		switch (Method) {

		case "Start_Execution":
			TestScriptStatus = "True";
			OtherSupportingMethod.Start_Execution();
			break;

		case "login_ECM":
			TestScriptStatus = "True";
			String username = GetTestData.GetRelativeTestData("UserName");
			String password = GetTestData.GetRelativeTestData("Password");
			TestScriptStatus = EcmAPImethods.login_ECM(username, password);
			break;
		case "create_Scope":
			TestScriptStatus = "True";
			String targetValue = GetTestData.GetRelativeTestData("TargetValue");
			String MainPD = GetTestData.GetRelativeTestData("MainPD");
			TestScriptStatus = EcmAPImethods.create_Scope(targetValue, MainPD);
			break;
		case "create_EC":
			TestScriptStatus = "True";
			TestScriptStatus = EcmAPImethods.create_EC();
			break;
		case "add_EC_Desc":
			TestScriptStatus = "True";
			String ECDesc = GetTestData.GetRelativeTestData("EcDescription");
			TestScriptStatus = EcmAPImethods.add_EC_Desc(ECDesc);
			break;
		case "getRouteDetails":
			TestScriptStatus = "True";
			MainPD = GetTestData.GetRelativeTestData("MainPD");
			TestScriptStatus = EcmAPImethods.getRouteDetails(MainPD);
			break;
		case "updateCarrierCategory":
			TestScriptStatus = "True";
			MainPD = GetTestData.GetRelativeTestData("MainPD");
			String OperationNo = GetTestData.GetRelativeTestData("OperationNo");
			String protocalZone = GetTestData.GetRelativeTestData("ProtocolZone");
			TestScriptStatus = EcmAPImethods.updateCarrierCategory(MainPD,OperationNo,protocalZone);
			break;
		case "getLRAID":
			TestScriptStatus = "True";
			String OperationPD = GetTestData.GetRelativeTestData("OperationPD");
			String ContextID = GetTestData.GetRelativeTestData("getLRAContextID");
			TestScriptStatus = EcmAPImethods.getLRAID(OperationPD,ContextID);
			break;
		case "deleteLRAwithPayload":
			TestScriptStatus = "True";
			OperationPD = GetTestData.GetRelativeTestData("OperationPD");
			TestScriptStatus = EcmAPImethods.deleteLRAwithPayload(OperationPD);
			break;
		case "verifyChangeSummary":
			TestScriptStatus = "True";
			replacePattern = GetTestData.getReplacePattern("ChangeSummary");
			String ExpectedChangeSummary = GetTestData.GetRelativeTestData("ChangeSummaryExpected");
			//System.out.println("Replace Pattern for Change Summary "+replacePattern);
			//System.out.println("Expected Change Summary "+ExpectedChangeSummary);
			
			TestScriptStatus = EcmAPImethods.verifyChangeSummary(ExpectedChangeSummary,replacePattern);
			break;
		case "verifyImpactSummary":
			TestScriptStatus = "True";
			replacePattern = GetTestData.getReplacePattern("ImpactSummary");
			String ExpectedImpactSummary = GetTestData.GetRelativeTestData("ImpactSummaryExpected");
			TestScriptStatus = EcmAPImethods.verifyImpactSummary(ExpectedImpactSummary,replacePattern);
			break;
		case "verifyValidationRule":
			TestScriptStatus = "True";
			replacePattern = GetTestData.getReplacePattern("ValidationRule");
			String ExpectedValidation = GetTestData.GetRelativeTestData("ValidationRuleExpected");
			TestScriptStatus = EcmAPImethods.verifyValidationRule(ExpectedValidation,replacePattern);
			break;
		case "getDependencyId":
			TestScriptStatus = "True";
			TestScriptStatus = EcmAPImethods.getDependencyID();
			break;
		case "updateDependency":
			TestScriptStatus = "True";
			String dependencyName = GetTestData.GetRelativeTestData("DependencyName");
			String dependencyDesc = GetTestData.GetRelativeTestData("DependencyDesc");
			TestScriptStatus = EcmAPImethods.updateDependency(dependencyName,dependencyDesc);
			break;
		case "submitEC":
			TestScriptStatus = "True";
			TestScriptStatus = EcmAPImethods.submitEC();
			break;
		case "approveEC":
			TestScriptStatus = "True";
			TestScriptStatus = EcmAPImethods.approveEC();
			break;
		case "getTaskId":
			TestScriptStatus = "True";
			String taskname = GetTestData.GetRelativeTestData("TaskName");
			TestScriptStatus = EcmAPImethods.getTaskId(taskname);
			break;
		case "completeTask":
			TestScriptStatus = "True";
			TestScriptStatus = EcmAPImethods.completeTask();
			break;
		case "getRBTVerificationTaskId":
			TestScriptStatus = "True";
			TestScriptStatus = EcmAPImethods.getRBTVerificationTaskId();
			break;
		case "siviewDBVerify":
			TestScriptStatus = "True";
			TestScriptStatus = SiviewDBConnect.siviewDBVerify();
			break;
		case "createAndUpdateMassLRA":
			TestScriptStatus = "True";
			String OperationPd = GetTestData.GetRelativeTestData("OperationPD");
			String MassLRACount = GetTestData.GetRelativeTestData("massLRACreateCount");
			
			String MassLRAContextType = GetTestData.GetRelativeTestData("updateLRAContextType");
			String MassLRACotextID = GetTestData.GetRelativeTestData("updateLRAContextID");
			String MassLogicalRecipe = GetTestData.GetRelativeTestData("updateLogicalRecipe");
			
			TestScriptStatus = EcmAPImethods.createAndUpdateMassLRA(OperationPd,MassLRACount,MassLRAContextType,MassLRACotextID,MassLogicalRecipe);
			break;
		case "getPDDetails":
			TestScriptStatus = "True";
			OperationPd = GetTestData.GetRelativeTestData("OperationPD");
			TestScriptStatus = EcmAPImethods.getPDDetails(OperationPd);
			break;
		case "updateExistingLRA":
			TestScriptStatus = "True";
			OperationPd = GetTestData.GetRelativeTestData("OperationPD");
			String LRAContextType = GetTestData.GetRelativeTestData("getLRAContextType");
			String LRAContextID = GetTestData.GetRelativeTestData("getLRAContextID");
			String LogicalRecipe = GetTestData.GetRelativeTestData("getLogicalRecipe");
			TestScriptStatus = EcmAPImethods.updateExistingLRA(OperationPd, LRAContextType, LRAContextID, LogicalRecipe);
			break;
		}	
		
		return TestScriptStatus;	
	}
}
