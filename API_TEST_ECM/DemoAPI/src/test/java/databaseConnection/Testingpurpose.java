package databaseConnection;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import org.testng.annotations.Test;

public class Testingpurpose {
	

public void TestReplace() throws IOException {
	
	String ProjectPath = System.getProperty("user.dir");
	String TestFilePath = ProjectPath+File.separator+"TestData\\ChangeSummaryExample.txt";
	String content = new String(Files.readAllBytes(Paths.get(TestFilePath)));
	
	//String s = "{timestamp:2019-03-19T10:53:43.918Z,total:52";
	String patt = "[\"timestamp\":\".*?\",\"total\\\"]";
	String Replacewith = "\"timestamp\":\"\",\"total\"";
	
	String patt2 = "\"total\":.*,";
	String Replacewith2 = "\"total\":,";
	
	//String Actual = s.replaceAll("\n","");
	String Output = content.replaceAll(patt,Replacewith);
	System.out.println(Output);
}
	
	
	
@Test	
public static void main(String[] args) {
	String str = "Bangalore[50]";
	String pattern = "[ag]";
	String str2;

	str2 = str.replaceAll(pattern, "pq");

	System.out.println("Given  String : " + str);
	System.out.println("Output String : " + str2);
}
	
	
	
	
	
}
