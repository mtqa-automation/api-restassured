package databaseConnection;
import org.testng.annotations.Test;
import java.sql.*;
public class SiviewDBConnect {
	@Test

	public static String siviewDBVerify () {
		String str = null;
		String MethodStatus = "True";

		try {
			String driver ="oracle.jdbc.driver.OracleDriver";
			String url="jdbc:oracle:thin:@fc8orag05:1521/smds1adc.gfoundries.com";
			String username= "smds_reader";
			String password= "smds_reader";

			Class.forName(driver);


			//Establish the connection
			Connection con= DriverManager.getConnection(url, username, password);
			System.out.println("Connection Sucess");
			Statement stmt= con.createStatement();
			ResultSet resultSet = stmt.executeQuery("select D_THESYSTEMKEY,D_THETABLEMARKER,MAINPD_IDENT,JOINING_OPER_NO from SMDS_ADMIN.FBMDPD_PO_RWKPD where D_THESYSTEMKEY = 'AUTO-UAT-CO4-RWK-SHARED-MODPD04.01'");
			System.out.println("SQL query Executed");
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			while (resultSet.next()) {
				System.out.println("Fetching all cell values from DB for Single row");
				String str1 = null;
				for (byte b = 1; resultSetMetaData.getColumnCount() >= b; b++) {
					if (str1 == null) {
						str1 = resultSet.getString(b);
					} else {
						str1 = str1 + "," + resultSet.getString(b);
					} 
				} 
				System.out.println("RowValues: " + str1);
				if (str == null) {
					str = str1;
					continue;
				} 
				str = str + "//" + str1;
			} 
			System.out.println("SiView DB result is "+str);


			/*while(resultSet.next())
        {
        System.out.println(resultSet.getString("D_THESYSTEMKEY")+","+resultSet.getString("D_THETABLEMARKER")+","+","+","+resultSet.getString("MAINPD_IDENT")+","+resultSet.getString("JOINING_OPER_NO"));
        }
			 */
			con.close();


		}catch (Exception e){
			//System.out.println("Exception for login_ECM "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}
	
}

