package driverPack;

import java.io.File;  
import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import getDataFromExcel.GetTestScript;


public class DriverClass { 

	public static ExtentReports report;
	public static ExtentTest logger;	
	public static String TCID ;
	public static String TCName;
	public static String TestDataID;
	public static String TCDescription;
	static String MethodName;
	public static String ScenarioName;

	static String TestCaseStatus;
	static String MethodStatus;
	static String ScenarioStatus;

	//Pass below Data in properties file and assign values here
	static String TestScriptFileName = "ECM_TestScript.xlsx";
	static String Environment  = "fc8ecmuv";
	static String HostName  = "LocalMachine";
	static String UserName  = "LoggedUserName";

	@BeforeTest
	public void startTest() {
		String ProjectPath = System.getProperty("user.dir");
		report = new ExtentReports(ProjectPath+File.separator+"Reports\\Report.html",true);
		report.addSystemInfo("Host Name",HostName)
		.addSystemInfo("Environment", Environment)
		.addSystemInfo("User Name", UserName);
		report.loadConfig(new File(ProjectPath+"\\extent-config.xml"));	
	}


	@Test
	public static void DriveExcel() throws Exception  { 
		ScenarioStatus = "True";
		String ProjectPath = System.getProperty("user.dir");
		String TestScriptPath = ProjectPath+File.separator+"TestScript\\"+TestScriptFileName;
		FileInputStream dataFilePath = new FileInputStream (TestScriptPath);

		XSSFWorkbook workbook = new XSSFWorkbook(dataFilePath);   
		XSSFSheet sheet = workbook.getSheet("TestScenarios");
		int rowCount = sheet.getLastRowNum();

		for(int i=1;i<=rowCount;i++){
			Row Executerows=sheet.getRow(i);
			Cell Executecell=Executerows.getCell(0);
			String ScenarioExecute=Executecell.getStringCellValue(); 

			if (ScenarioExecute.equals("Y")) {
				Row ScenarioRow=sheet.getRow(i);
				Cell ScenarioCell=ScenarioRow.getCell(2);
				ScenarioName=ScenarioCell.getStringCellValue(); 

				ScenarioStatus = getExecutableTestCase();

				if (ScenarioStatus.equalsIgnoreCase("False")) {
					//break;
					//logger.log(LogStatus.FAIL, "Scenario "+ScenarioName+" Failed");
				}
				else {
					//logger.log(LogStatus.PASS, "Scenario "+ScenarioName+" Passed");
					continue;
				}
			}
		}
		workbook.close();
	}  


	@Test
	public static String getExecutableTestCase() throws Exception {

		TestCaseStatus = "True";

		String ProjectPath = System.getProperty("user.dir");
		String TestScriptPath = ProjectPath+File.separator+"TestScript\\"+TestScriptFileName;
		FileInputStream dataFilePath = new FileInputStream (TestScriptPath);

		XSSFWorkbook workbook = new XSSFWorkbook(dataFilePath);   
		XSSFSheet sheet = workbook.getSheet(ScenarioName);    
		int rowCount = sheet.getLastRowNum();

		for(int j=1;j<=rowCount;j++){

			Row ExecuteTC=sheet.getRow(j);
			Cell ExecuteTcCell=ExecuteTC.getCell(0); 
			String TcExecute=ExecuteTcCell.getStringCellValue(); 

			if (TcExecute.equals("Y")) {
				Row TCIDRow=sheet.getRow(j); 
				Cell TCIDCell=TCIDRow.getCell(1); 
				Cell TCNameCell = TCIDRow.getCell(2);
				Cell TcDesc = TCIDRow.getCell(3);

				TCID=TCIDCell.getStringCellValue();
				TCName = TCNameCell.getStringCellValue();
				TCDescription = TcDesc.getStringCellValue();

				logger = report.startTest(ScenarioName+" - "+TCID);

				TestCaseStatus = getExecutableMethod ();

				if (TestCaseStatus.equalsIgnoreCase("False")) {
					System.out.println("Status For TC "+TCID+" is: "+TestCaseStatus+"\n");
					//logger.log(LogStatus.FAIL, "Test case "+TCID+" Failed");
					//break;
				}
				else {
					System.out.println("Status For TC "+TCID+" is: "+TestCaseStatus+"\n");
					//logger.log(LogStatus.PASS, "Test case "+TCID+" passed");
					continue;
				}

			}

		}

		workbook.close();
		return TestCaseStatus;
	}


	@Test
	public static String getExecutableMethod() throws Exception{

		MethodStatus = "True";

		String ProjectPath = System.getProperty("user.dir");
		String TestScriptPath = ProjectPath+File.separator+"TestScript\\"+TestScriptFileName;
		FileInputStream dataFilePath = new FileInputStream (TestScriptPath);

		XSSFWorkbook workbook = new XSSFWorkbook(dataFilePath);   
		XSSFSheet sheet = workbook.getSheet(ScenarioName);    
		int rowCount = sheet.getLastRowNum();

		for(int k=1;k<=rowCount;k++){

			Row getMethodRow=sheet.getRow(k); 
			Cell getMethodCell=getMethodRow.getCell(1);
			String getTCID=getMethodCell.getStringCellValue(); 

			if (getTCID.equalsIgnoreCase(TCID)) {

				Row MethodRow=sheet.getRow(k);
				Cell MethodCell=MethodRow.getCell(4);
				Cell MethodDataID= MethodRow.getCell(5);

				String MethodName = MethodCell.getStringCellValue();
				TestDataID = MethodDataID.getStringCellValue();
				MethodStatus = GetTestScript.ExecuteMethods(MethodName);

				if (MethodStatus.equalsIgnoreCase("False")) {
					//System.out.println("Method "+MethodName+" Status is: "+MethodStatus);
					logger.log(LogStatus.FAIL, "Method "+MethodName+" Failed");
					break;
				}
				else {
					logger.log(LogStatus.PASS, "Method "+MethodName+" Passed");
					continue;
				}
			}
		}
		workbook.close();
		return MethodStatus;
	}

	@AfterMethod
	public static void getResult (ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE ) {
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getName());
			logger.log(LogStatus.FAIL, "Test Case Failed is "+result.getThrowable());
		}else if (result.getStatus() == ITestResult.SKIP ) {
			logger.log(LogStatus.SKIP, "Test Case skipped is "+result.getName());
		}
		report.endTest(logger);
	}

	@AfterClass
	public void endReport() {
		report.flush();
		//report.close();

	}

}



