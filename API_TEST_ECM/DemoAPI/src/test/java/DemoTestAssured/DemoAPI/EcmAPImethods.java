package DemoTestAssured.DemoAPI;

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class EcmAPImethods {

	static String Hostname = "fc8ecmtv";

	static RequestSpecification httpRequest;
	static  Response response;
	static String res;
	static ObjectMapper mapper;
	static JsonNode jnode;
	static JsonPath jsonPathValidator; 
	static List<JsonNode> jsonResponseRoot;

	static int StepCount = 1;
	static String TokenVal = null;
	static String ScopeID = null;
	static String associationId = null;
	static String taskId;
	static String ECID;
	static String EcValue;
	static String Revision;
	static String depId;
	static String MethodStatus;
	static int nodeCount;


	public static String login_ECM(String userName,String Password ) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			//API for Login ECM with user name and password
			RestAssured.urlEncodingEnabled = false;
			RestAssured.baseURI = "http://"+Hostname+"/api/auth/login";
			httpRequest = RestAssured.given().auth().preemptive().basic(userName, Password)
					// .contentType("application/json").log().all();
					.contentType("application/json");
			response = httpRequest.post();
			System.out.println("Step " +StepCount+++ ": Login ECM APP");
			System.out.println("*************************************");
			TokenVal = response.header("Authorization");
			System.out.println("Token Value is: " + TokenVal + "\n");
			assertEquals("compare logged username", userName, response.path("username"));
			assertEquals("compare status code", 200, response.getStatusCode());
		}

		catch (Exception e){
			//System.out.println("Exception for login_ECM "+e);
			MethodStatus = "False";
		}

		return MethodStatus;

	}
	//create_Scope method uses TokenVal from login_ECM method
	//create_Scope method uses TargetVal and MainPD from Test data
	//create_Scope method returns ScopeID	
	public static String create_Scope(String TargetVal, String MainPD) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try { 
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/scopes";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"actions\":[{\"target\":[\""+TargetVal+"\"],\"operation\":\"add\",\"data\":\""+MainPD+"\"}]}");
			response =httpRequest.post();
			System.out.println("Step " +StepCount+++ ": Create Scope");
			System.out.println("************************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res); 
			ScopeID = jnode.findValue("objectId").asText();
			System.out.println("ScopeID Value is: "+ScopeID+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch (Exception e) {
			//System.out.println("Exception for create_Scope "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}
	//create_EC uses TokenVal from login_ECM method
	//create_EC uses ScopeID value from create_Scope method	
	//create_EC returns ECValue and ECID 	
	public static String create_EC() throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"ecType\":\"ROUTE\",\"scopeId\":\""+ScopeID+"\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++ ": Create EC");
			System.out.println("********************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res);
			ECID = jnode.findValue("objectId").asText();
			System.out.println("ECID is: "+ECID);
			EcValue = jnode.findValue("ecId").asText();
			System.out.println("EC Value is: "+EcValue+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch (Exception e) {
			//System.out.println("Exception for create_EC "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}
	//add_EC_Desc uses ECID from create_EC method
	//add_EC_Desc takes input from Testdata sheet
	public static String add_EC_Desc(String ECDesc) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {  
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"actions\":{\"operation\":\"editDescription\",\"data\":[\""+ECDesc+"\"]}}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++": Add EC Description");
			System.out.println("****************************************");
			System.out.println("Status for Add Desc is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for add_EC_Desc "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}


	public static String getRouteDetails(String MainPD) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {  
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/routes/"+MainPD+"/" + ECID;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal);    		  			
			response =httpRequest.get();
			System.out.println("Step "+StepCount+++": Get Route Details");
			System.out.println("***************************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res);
			Revision = jnode.findValue("revision").asText();
			System.out.println("Revision Value is: "+Revision);
			System.out.println("Status for Get Route is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch (Exception e) {
			//System.out.println("Exception for getRouteDetails "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String updateCarrierCategory(String MainPD,String OperationNo, String protocolzone ) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/mainpds/"+MainPD+"/"+ECID+"/"+Revision+"/operations/"+OperationNo;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"protocolZone\":\""+protocolzone+"\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++": Update Carrier Category");
			System.out.println("*********************************************");
			System.out.println("Status for update carrier category is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for updateCarrierCategory "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}


	//Get LRA ID

	public static String getLRAID(String OperationPD,String ContextID) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/processdefinitions/"+OperationPD+"/"+ECID;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal); 
			response =httpRequest.get();
			res = response.getBody().asString(); 
			jnode = mapper.readTree(res);
			System.out.println("Step "+StepCount+++":API  - Get LRAID");
			System.out.println("*************************************");
			System.out.println("Response Recived"+res);
			System.out.println("Status for getLRAID is: "+response.getStatusCode()+"\n");
			JsonPath jsonPathValidator = response.jsonPath();
			List<JsonNode> jsonResponseRoot = jsonPathValidator.getList("logicalRecipes");
			int nodeCount = jsonResponseRoot.size();
			//System.out.println("Number of Nodes : " + jsonResponseRoot.size());
			for(int i = 1; i < nodeCount+1; i++) {
				String originalValue = response.jsonPath().getString("logicalRecipes.context.originalValue.value"+"["+i+"]");
				System.out.println("get context ID "+originalValue);
				if (originalValue.equalsIgnoreCase(ContextID) ) {
					associationId = response.jsonPath().getString("logicalRecipes.associationId"+"["+i+"]");
					System.out.println(associationId);
					System.out.println("got values");
					break;
				}
			}

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}



	public static String deleteLRAwithPayload(String OperationPD) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";
		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/processdefinitions/"+OperationPD+"/"+ECID+"/"+Revision+"/logicalrecipes";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"pdIdsAndLras\":[{\"key\":\""+OperationPD+"\",\"values\":[{\"id\":\""+associationId+"\"}]}],\"type\":1}");
			response =httpRequest.delete();
			System.out.println("Step "+StepCount+++":deleteLRAwithPayload");
			System.out.println("*****************************************");
			System.out.println("Status for deleteLRAwithPayload is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String verifyChangeSummary(String ExpectedChangeSummary,String replacePattern) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/changesummary";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal);    		  			
			response =httpRequest.get();
			
			System.out.println("Step "+StepCount+++":Get ChangeSummary");
			System.out.println("**************************************");
			res = response.getBody().asString(); 
			String ActualChangeSummary = res;
			jnode = mapper.readTree(res);

			System.out.println("Response Body Change Summary "+res);
			
			System.out.println("Status for Get Route is "+response.getStatusCode());
			assertEquals("compare status code",200,response.getStatusCode());

			String getExpectedChangeSummary = OtherSupportingMethod.replaceWithReplacePattern(ExpectedChangeSummary,replacePattern);
			String getActualChangeSummary = OtherSupportingMethod.replaceWithReplacePattern(ActualChangeSummary,replacePattern);
			
			System.out.println("Expected Replaced Change Summary "+getExpectedChangeSummary);
			System.out.println("Actual Replaced Change Summary "+getActualChangeSummary);
			
			boolean blnFlag = OtherSupportingMethod.compareValues(getExpectedChangeSummary,getActualChangeSummary);

			if (blnFlag == true) {
				MethodStatus = "True";
				System.out.println("Actual Change summary matches with expected change summary"+"\n");
			}
			else if (blnFlag == false) {
				MethodStatus = "False";
			}
		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			//MethodStatus = "False";
		}
		return MethodStatus;
	}	


	public static String verifyImpactSummary(String ExpectedImpactSummary,String replacePattern) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/impact/summary";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal);    		  			
			response =httpRequest.get();
			System.out.println("Step "+StepCount+++":Get Impact");
			System.out.println("*******************************");
			res = response.getBody().asString(); 
			String ActualImpactSummary = res;
			jnode = mapper.readTree(res);
			
			System.out.println("Response Body Impact Summary "+res);
			
			System.out.println("Status for Get Route is "+response.getStatusCode());
			assertEquals("compare status code",200,response.getStatusCode());
			
			String getExpectedImpactSummary = OtherSupportingMethod.replaceWithReplacePattern(ExpectedImpactSummary,replacePattern);
			String getActualImpactSummary = OtherSupportingMethod.replaceWithReplacePattern(ActualImpactSummary,replacePattern);

			System.out.println("Expected Replaced Impact Summary "+getExpectedImpactSummary);
			System.out.println("Actual Replaced Summary "+getActualImpactSummary);
			
			boolean blnFlag = OtherSupportingMethod.compareValues(getExpectedImpactSummary,getActualImpactSummary);

			if (blnFlag == true) {
				MethodStatus = "True";
				System.out.println("Actual impact summary matches with expected impact summary"+"\n");
			}
			else if (blnFlag == false) {
				MethodStatus = "False";
			}

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String verifyValidationRule(String ExpectedValidationRule,String replacePattern) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/validation";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal); 

			response =httpRequest.get();
			System.out.println("Step "+StepCount+++":Get Validation");
			System.out.println("***********************************");
			res = response.getBody().asString(); 
			String ActualValidationRule = res;
			jnode = mapper.readTree(res);

			System.out.println("Response Body ValidationRule "+res);
			
			System.out.println("Status for Get Route is "+response.getStatusCode());
			assertEquals("compare status code",200,response.getStatusCode());

			String getExpectedValidationRule = OtherSupportingMethod.replaceWithReplacePattern(ExpectedValidationRule,replacePattern);
			String getActualValidationRule = OtherSupportingMethod.replaceWithReplacePattern(ActualValidationRule,replacePattern);
			
			System.out.println("Expected Replaced Validation :" +getExpectedValidationRule);
			System.out.println("Actual Replaced Validation :"+getActualValidationRule);
			
			boolean blnFlag = OtherSupportingMethod.VerifySubstringInString(getExpectedValidationRule,getActualValidationRule);

			if (blnFlag == true) {
				MethodStatus = "True";
				System.out.println("Actual validation rule matches with expected validation rule"+"\n");
			}
			else if (blnFlag == false) {
				MethodStatus = "False";
			}
		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String getDependencyID() throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/dependencies";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal);    		  			
			response =httpRequest.get();
			System.out.println(" Get Dependency ID");
			System.out.println("Step "+StepCount+++":Get Dependency ID");
			System.out.println("**************************************");
			res = response.getBody().asString(); 
			jnode = mapper.readTree(res);
			depId = jnode.findValue("objectId").asText();
			System.out.println("DependencyID Value is "+depId);
			System.out.println("Status for Get Dependency ID is "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String updateDependency(String DependencyName, String DependencyDescription) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/dependencies/"+depId;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"name\":\""+DependencyName+"\",\"description\":\""+DependencyDescription+"\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++":API - Add EC Dependency values");
			System.out.println("***************************************************");
			System.out.println("Status for Add Desc is "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String submitEC() throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/submit";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"neededBy\":\"2020-07-08T18:30:00.000Z\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++":API - Submit EC");
			System.out.println("************************************");
			System.out.println("Status for submitEC is "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String approveEC() throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/approve";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"comment\": \"ApproveED\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++":API - Approve EC");
			System.out.println("*************************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res); 
			System.out.println("Status for approveEC is "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}


	//Get RBT PresetupTaskID
	public static String getTaskId(String taskname) throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/tasks";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal); 
			response =httpRequest.get();
			System.out.println("Step "+StepCount+++":API - Get TaskID");
			System.out.println("*************************************");
			res = response.getBody().asString(); 
			jnode = mapper.readTree(res);
			System.out.println("Response Recived"+res);
			JsonPath jsonPathValidator = response.jsonPath();

			jsonResponseRoot = jsonPathValidator.getList("data");
			nodeCount = jsonResponseRoot.size();
			System.out.println("Number of Nodes : " + jsonResponseRoot.size());
			for(int i = 1; i < nodeCount+1; i++) {

				String taskStatus = response.jsonPath().getString("data.enabled"+"["+i+"]");  
				String taskName = response.jsonPath().getString("data.data.name"+"["+i+"]");
				if (taskStatus.equalsIgnoreCase("true") ) {	
					if(taskName.equalsIgnoreCase(taskname))
					{
						taskId = response.jsonPath().getString("data.objectId"+"["+i+"]");
						System.out.println(taskId);
						System.out.println("got values"+"\n");
						break;
					}
				}
			}

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String getRBTVerificationTaskId() throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/tasks";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal); 
			response =httpRequest.get();
			System.out.println("Step "+StepCount+++":API - getRBTVerificationTaskId");
			System.out.println("***************************************************");
			res = response.getBody().asString(); 
			jnode = mapper.readTree(res);
			System.out.println("Response Recived"+res);
			JsonPath jsonPathValidator = response.jsonPath();
			
			jsonResponseRoot = jsonPathValidator.getList("data");
			nodeCount = jsonResponseRoot.size();
			System.out.println("Number of Nodes : " + jsonResponseRoot.size());
			for(int i = 1; i < nodeCount+1; i++) {

				String taskStatus = response.jsonPath().getString("data.enabled"+"["+i+"]");  
				String taskName = response.jsonPath().getString("data.data.name"+"["+i+"]");
				if (taskStatus.equalsIgnoreCase("true") ) {	
					if(taskName.equalsIgnoreCase("RBT Verification"))
					{
						taskId = response.jsonPath().getString("data.objectId"+"["+i+"]");
						System.out.println(taskId);
						System.out.println("got values"+"\n");
						break;
					}
				}
			}

		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String completeTask() throws JsonMappingException, JsonProcessingException{
		MethodStatus = "True";

		try {

			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID+"/tasks/"+taskId+"/complete";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"comment\": \"complete task\"}");
			response =httpRequest.post();
			System.out.println("Complete RBT Presetup");
			System.out.println("Step "+StepCount+++":API - Complete RBT Presetup");
			System.out.println("************************************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res); 
			System.out.println("Status for Complete RBT Presetup is "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String getPDDetails(String OperationPD) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled = false;
			RestAssured.baseURI = "http://" + Hostname + "/api/processdefinitions/" + OperationPD + "/" + ECID;
			httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal);
			response = httpRequest.get();
			System.out.println("Step " + StepCount++ + ": Get PD Details");
			System.out.println("****************************************");
			res = response.getBody().asString();
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res);
			Revision = jnode.findValue("revision").asText();
			System.out.println("Revision Value is: " + Revision);
			System.out.println("Status for Get Route is: " + response.getStatusCode() + "\n");
			assertEquals("compare status code", 200, response.getStatusCode());
		} catch (Exception e) {
			// System.out.println("Exception for getRouteDetails "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}
	
	public static String createAndUpdateMassLRA(String OperationPd,String MassLRACount,String MassLRAContextType,String MassLRACotextID,String MassLogicalRecipe ) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			  RestAssured.urlEncodingEnabled= false;
			  RestAssured.baseURI  =  "http://fc8ecmtv/api/processdefinitions/"+OperationPd+"/"+ECID+"/"+Revision+"/logicalRecipes";
			  httpRequest = RestAssured
			  .given()
			  .header("Authorization","Bearer "+TokenVal)
			  .body("{\"createAmount\":"+MassLRACount+"}");
			  response =httpRequest.post();
			  System.out.println("Step "+StepCount+++":create And Update Mass LRA");
			  System.out.println("***********************************************");
			  res = response.getBody().asString();
			  mapper = new ObjectMapper();
			  jnode = mapper.readTree(res);

			 JsonParser parser = new JsonParser();
			 JsonObject jsonObj  = parser.parse(res).getAsJsonObject();
			 JsonArray fieldsObject = jsonObj.getAsJsonArray("ids");
			 
			 String [] LRAContextType = MassLRAContextType.split("/");
			 String[] LRAContextID = MassLRACotextID.split("/");
			 String[] LogicalRecipe = MassLogicalRecipe.split("/");
			 
			 int updatedRevID = Integer.parseInt(Revision)+Integer.parseInt(MassLRACount);
			 
			 //System.out.println("Rev ID"+updatedRevID);
			 
			 for (int i = 0;i<fieldsObject.size();i++) {
				 String getID = fieldsObject.get(i).getAsString(); 
				 
				RestAssured.baseURI = "http://fc8ecmtv/api/processdefinitions/"+OperationPd+"/"+ECID+"/"+updatedRevID+"/logicalRecipes/"+getID;
				httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
							.body("{\"contextType\":"+LRAContextType[i]+",\"contextValue\":\""+LRAContextID[i]+"\",\"logicalRecipe\":\""+LogicalRecipe[i]+"\"}");
				response = httpRequest.post();
				//System.out.println("Status for update LRA is: " + response.getStatusCode() + "\n");
				System.out.println("Status for Get Route is: " + response.getStatusCode() + "\n");
				assertEquals("compare status code", 200, response.getStatusCode());
				updatedRevID = updatedRevID+1;
			 }	
		} catch (Exception e) {
			//System.out.println("Exception for MassUpdateLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}
	
	
	public static String updateExistingLRA(String OperationPD, String LRAContextType, String LRAContextID, String LogicalRecipe) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled = false;
			RestAssured.baseURI = "http://" + Hostname + "/api/processdefinitions/"+OperationPD+"/"+ECID+"/"+Revision+"/logicalRecipes/"+associationId;
			httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
					.body("{\"contextType\":"+LRAContextType+",\"contextValue\":\""+LRAContextID+"\",\"logicalRecipe\":\""+LogicalRecipe+"\"}");
			response = httpRequest.post();
			System.out.println("Step " + StepCount++ + ": updateExistingLRA");
			System.out.println("*******************************************");
			System.out.println("Status for update LRA is: " + response.getStatusCode() + "\n");
			assertEquals("compare status code", 200, response.getStatusCode());
		} catch (Exception e) {
			// System.out.println("Exception for updateCarrierCategory "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}
	
	
	

}