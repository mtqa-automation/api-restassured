package DemoTestAssured.DemoAPI;

import java.util.regex.Pattern;

import com.relevantcodes.extentreports.LogStatus;

import driverPack.DriverClass;

public class OtherSupportingMethod {


	static String MethodStatus;

	public static void Start_Execution() {

		DriverClass.logger.log(LogStatus.PASS, "Scenario: "+DriverClass.ScenarioName+"||TCID: "+DriverClass.TCID);

	}


	public static String replaceWithReplacePattern(String paramString1, String ReplacePattern) {
		MethodStatus = "True";
		try {
			paramString1 = mergeCellValues(paramString1);
			String[] arrayOfString = ReplacePattern.split("\n");
			for (String str : arrayOfString) {
				String[] arrayOfString1 = str.split("==>");
				Pattern pattern = Pattern.compile(arrayOfString1[0], 34);
				paramString1 = pattern.matcher(paramString1).replaceAll(arrayOfString1[1]);
				//System.out.println("After replace"+paramString1);
			}
		}catch (Exception e){
			//System.out.println("Exception for login_ECM "+e);
			MethodStatus = "False";
		}

		return paramString1;
	}


	public static String mergeCellValues(String paramString) {
		String str = "";
		try {
			if (null != paramString) {
				String[] arrayOfString = paramString.split("\n");
				for (String str1 : arrayOfString)
					str = str + str1; 
			}

		}catch (Exception e){
			//System.out.println("Exception for login_ECM "+e);
		}
		return str;
	}



	public static boolean compareValues (String ExpectedValue, String ActualValue) {
		boolean blnFlag = true;

		if (ExpectedValue .equals(ActualValue)) {
			blnFlag = true;

		}

		else {
			blnFlag = false;
		}


		return blnFlag;
	}
	
	
	public static boolean VerifySubstringInString(String ExpectedValue, String ActualValue) {
		boolean blnFlag = true;
		
		if (ActualValue.contains(ExpectedValue)) {
		
			blnFlag = true;
		}
		else {
			blnFlag = false;
		}
		return blnFlag;
	}
	

}
