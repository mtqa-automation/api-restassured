package DemoTestAssured.DemoAPI;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Array;
import java.util.List;

import org.json.JSONArray;
import org.testng.annotations.Test;

import com.fasterxml.jackson.annotation.JsonFormat.Value;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ECM_E2E_POC_TC01 {

	@Test

	public static void LoginECM() throws JsonMappingException, JsonProcessingException {

		// API for Login ECM with user name and password
		System.out.println("API Testing : RestAssured");
		System.out.println("Developed By : @mit & @njali");
		System.out.println("=============================" + "\n");

		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/auth/login";
		RequestSpecification httpRequest = RestAssured.given().auth().preemptive().basic("arout1", "test123")
				// .contentType("application/json").log().all();
				.contentType("application/json");
		Response response = httpRequest.post();
		System.out.println("Step 1 - Login ECM APP");
		System.out.println("************************");
		String TokenVal = response.header("Authorization");
		System.out.println("Token Value is: " + TokenVal + "\n");
		assertEquals("compare logged username", "arout1", response.path("username"));
		assertEquals("compare status code", 200, response.getStatusCode());

		// API for Create Scope
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/scopes";
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
				.body("{\"actions\":[{\"target\":[\"MNPD\"],\"operation\":\"add\",\"data\":\"QA-MPD001.01\"}]}");
		response = httpRequest.post();
		System.out.println("Step 2 - Create Scope");
		System.out.println("************************");
		String res = response.getBody().asString();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jnode = mapper.readTree(res);
		String ScopeID = jnode.findValue("objectId").asText();
		System.out.println("ScopeID Value is: " + ScopeID + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());

		// API for Create Engineering Change
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges";
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
				.body("{\"ecType\":\"ROUTE\",\"scopeId\":\"" + ScopeID + "\"}");
		response = httpRequest.post();
		System.out.println("Step 3 - Create EC");
		System.out.println("******************");
		res = response.getBody().asString();
		jnode = mapper.readTree(res);
		String ECID = jnode.findValue("objectId").asText();
		System.out.println("ECID is: " + ECID);
		String EcValue = jnode.findValue("ecId").asText();
		System.out.println("EC Value is: " + EcValue + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());

		// API for Add EC Description
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges/" + ECID;
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
				.body("{\"actions\":{\"operation\":\"editDescription\",\"data\":[\"test12345\"]}}");
		response = httpRequest.post();
		System.out.println("Step 4 - Add EC Description");
		System.out.println("***************************");
		System.out.println("Status for Add Desc is: " + response.getStatusCode() + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());

		// API for Get Route - store Revision ID
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/routes/QA-MPD001.01/" + ECID;
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal);
		response = httpRequest.get();
		System.out.println("Step 5 - Get Route Details");
		System.out.println("**************************");
		res = response.getBody().asString();
		jnode = mapper.readTree(res);
		String Revision = jnode.findValue("revision").asText();
		System.out.println("Revision Value is: " + Revision);
		System.out.println("Status for Get Route is: " + response.getStatusCode() + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());

		
		  //API for Change Carrier Category RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI = "http://fc8ecmtv/api/mainpds/QA-MPD001.01/"+ECID+"/"+Revision+"/operations/1000.2100"; 
		  httpRequest = RestAssured 
				  		.given()
				  		.header("Authorization","Bearer "+TokenVal)
				  		.body("{\"protocolZone\":\"BEOL\"}"); response =httpRequest.post();
		  System.out.println("Step 6 - Update Carrier Category");
		  System.out.println("********************************");
		  System.out.println("Status for update carrier category is: "+response.
		  getStatusCode()+"\n");
		  assertEquals("compare status code",200,response.getStatusCode());
		  
		  
		  
		  int LRACreteAmount = 3;
		  
		  RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI  =  "http://fc8ecmtv/api/processdefinitions/QA-PD002.01/"+ECID+"/"+Revision+"/logicalRecipes";
		  httpRequest = RestAssured
		  .given()
		  .header("Authorization","Bearer "+TokenVal)
		  .body("{\"createAmount\":"+LRACreteAmount+"}");
		  response =httpRequest.post();
		  System.out.println("Step: Create LRA one");
		  System.out.println("************************");
		  res = response.getBody().asString();
		  mapper = new ObjectMapper();
		  jnode = mapper.readTree(res);
		 
		 JsonParser parser = new JsonParser();
		 JsonObject jsonObj  = parser.parse(res).getAsJsonObject();
		 JsonArray fieldsObject = jsonObj.getAsJsonArray("ids");
		 
		 //System.out.println(fieldsObject.get(0).getAsString());
		 //System.out.println(fieldsObject.get(1).getAsString());
		 //System.out.println(fieldsObject.get(2).getAsString());
		 
		 //String opd = "QA-PD002.01/QA-PD002.01";
		 String OperationPD = "QA-PD002.01";
		 String LRContextType = "3/3/3";
		 String [] LRAContextType = LRContextType.split("/");
		 String LRCotextID = "QA-PG002/QA-PG001/QA-PG002";
		 String[] LRAContextID = LRCotextID.split("/");
		 String LogRcp = "QA-LOGRCP002.01/QA-LOGRCP002.01/QA-LOGRCP002.01";
		 String[] LogicalRecipe = LogRcp.split("/");
		 
		 int updatedRevID = Integer.parseInt(Revision)+LRACreteAmount;
		 //int updatedRevID = 2;
		 
		 for (int i = 0;i<fieldsObject.size();i++) {
			 String getID = fieldsObject.get(i).getAsString();
			 System.out.println(updatedRevID);
			 System.out.println(getID);
			// System.out.println(OperationPD[i]);
			 System.out.println(LRAContextType[i]);
			 System.out.println(LRAContextID[i]);
			 System.out.println(LogicalRecipe[i]);
			 
			RestAssured.urlEncodingEnabled = false;
			RestAssured.baseURI = "http://fc8ecmtv/api/processdefinitions/"+OperationPD+"/"+ECID+"/"+updatedRevID+"/logicalRecipes/"+getID;
			httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
						.body("{\"contextType\":"+LRAContextType[i]+",\"contextValue\":\""+LRAContextID[i]+"\",\"logicalRecipe\":\""+LogicalRecipe[i]+"\"}");
			response = httpRequest.post();
			System.out.println("Status for update LRA is: " + response.getStatusCode() + "\n");
			assertEquals("compare status code", 200, response.getStatusCode());
			updatedRevID = updatedRevID+1;
			System.out.println("rev id "+updatedRevID);
		 }
		 
		 
	/*	RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/processdefinitions/QA-PD002.01/"+ECID+"/"+Revision+"/logicalRecipes";
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
					.body("{\"createAmount\":1}");
		response = httpRequest.post();
					
		res = response.getBody().asString();
		jnode = mapper.readTree(res);
		String getCreateLraID= res.substring(9, 25);
		System.out.println("Step: Create LRA one");
		System.out.println("************************");
		
		System.out.println("Response Body:" +res);
		System.out.println("create ID Value is: " + getCreateLraID + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());
		
		
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://fc8ecmtv/api/processdefinitions/QA-PD002.01/"+ECID+"/"+Revision+"/logicalRecipes";
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
					.body("{\"createAmount\":1}");
		response = httpRequest.post();
					
		res = response.getBody().asString();
		jnode = mapper.readTree(res);
		String getCreateLraID2= res.substring(9, 25);
		System.out.println("Step: Create LRA Two");
		System.out.println("************************");
		
		System.out.println("Response Body:" +res);
		System.out.println("create ID Value is: " + getCreateLraID2 + "\n");
		assertEquals("compare status code", 200, response.getStatusCode()); 
		
		
		
		
		RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI  =  "http://fc8ecmtv/api/processdefinitions/QA-PD002.01/"+ECID+"/0/logicalRecipes";
		  httpRequest = RestAssured
		  .given()
		  .header("Authorization","Bearer "+TokenVal)
		  .body("{\"createAmount\":2}");
		  response =httpRequest.post();
		  System.out.println("Step: Create LRA one");
		  System.out.println("************************");
		  res = response.getBody().asString();
		  mapper = new ObjectMapper();
		  jnode = mapper.readTree(res);
		 
		 JsonParser parser = new JsonParser();
		 
		 JsonObject jsonObj  = parser.parse(res).getAsJsonObject();
		 JsonArray fieldsObject = jsonObj.getAsJsonArray("ids");
		 
		 String getCreateLraID = fieldsObject.get(0).getAsString();
		 String getCreateLraID2 = fieldsObject.get(1).getAsString();
		 //System.out.println(fieldsObject.get(2).getAsString());
		  

		String Hostname = "fc8ecmtv";
		Revision = "2";
		String OperationPD = "QA-PD002.01";
		String LRAContextType = "3";
		String LRAContextID = "QA-PG002";
		String LogicalRecipe = "QA-LOGRCP002.01";
		
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://" + Hostname + "/api/processdefinitions/"+OperationPD+"/"+ECID+"/"+Revision+"/logicalRecipes/"+getCreateLraID;		
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
					.body("{\"contextType\":"+LRAContextType+",\"contextValue\":\""+LRAContextID+"\",\"logicalRecipe\":\""+LogicalRecipe+"\"}");
		response = httpRequest.post();
		System.out.println("Step  : Update LRA1");
		System.out.println("********************************");
		System.out.println("Status for update LRA is: " + response.getStatusCode() + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());

		
		Hostname = "fc8ecmtv";
		Revision = "3";
		OperationPD = "QA-PD002.01";
		LRAContextType = "3";
		LRAContextID = "QA-PG001";
		LogicalRecipe = "QA-LOGRCP001.01";
		
		RestAssured.urlEncodingEnabled = false;
		RestAssured.baseURI = "http://" + Hostname + "/api/processdefinitions/"+OperationPD+"/"+ECID+"/"+Revision+"/logicalRecipes/"+getCreateLraID2;		
		httpRequest = RestAssured.given().header("Authorization", "Bearer " + TokenVal)
					.body("{\"contextType\":"+LRAContextType+",\"contextValue\":\""+LRAContextID+"\",\"logicalRecipe\":\""+LogicalRecipe+"\"}");
		response = httpRequest.post();
		System.out.println("Step  : Update LRA2");
		System.out.println("********************************");
		System.out.println("Status for update LRA is: " + response.getStatusCode() + "\n");
		assertEquals("compare status code", 200, response.getStatusCode());

		

		  
		  
		  
		  
		  
		  
		  /*
		  //Get LRA ID 
		  RestAssured.urlEncodingEnabled= false; 
		  RestAssured.baseURI ="http://fc8ecmtv/api/processdefinitions/QA-PD097.01/"+ECID; 
		  httpRequest = RestAssured 
				  		.given() 
				  		.header("Authorization","Bearer "+TokenVal); 
		  response = httpRequest.get(); 
		  System.out.println("Step 7  - Get LRAID");
		  System.out.println("*******************"); res =
		  response.getBody().asString(); jnode = mapper.readTree(res);
		  //System.out.println("Revision Value is "+res);
		  System.out.println("Status for Get Route is "+response.getStatusCode()+"\n");
		  System.out.println("Response Body is: "+ res);
		  assertEquals("compare status code",200,response.getStatusCode());
		  //List<JsonNode> LRAID = jnode.findValues("associationId"); 
		  //String associationId = response.jsonPath().getString("logicalRecipes.associationId[0]");
		  JsonPath jsonPathValidator = response.jsonPath(); 
		  List<JsonNode> jsonResponseRoot = jsonPathValidator.getList("logicalRecipes"); 
		  int nodeCount = jsonResponseRoot.size(); 
		  System.out.println("Number of Nodes : " + jsonResponseRoot.size());
		  
		  
		  String inputOriginalVal = "QA-PG001";
		  
		  for(int i = 1; i < nodeCount+1; i++) { 
			  //String originalValue = response.jsonPath().getString("logicalRecipes.logicalRecipe.originalValue"+"["+i+"]"); 
			  String originalValue =response.jsonPath().getString("logicalRecipes.context.originalValue.value"+"["+i+"]"); 
			  if (originalValue .equals(inputOriginalVal) ) { 
				  String associationId = response.jsonPath().getString("logicalRecipes.associationId"+"["+i+"]");
				  System.out.println("Association ID for originalValue: "+inputOriginalVal+" is: "+associationId); 
				 break; 
			  } 
		  } 
		  
		  
		  //API for Change summary and verify the change
		  RestAssured.urlEncodingEnabled= false; 
		  RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges/"+ECID+"/changesummary"; 
		  httpRequest = RestAssured 
				  		.given() 
				  		.header("Authorization","Bearer "+TokenVal); 
		  response = httpRequest.get(); System.out.println("Step 7 - Get ChangeSummary");
		  System.out.println("**************************"); res =
		  response.getBody().asString(); jnode = mapper.readTree(res);
		  System.out.println("Actual Change Summary Response is: "+res);
		  System.out.println("Status for Get Route is: "+response.getStatusCode()+"\n");
		  assertEquals("compare status code",200,response.getStatusCode());
		  
		  //API for Verify Impact RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges/"+ECID+"/impact/summary"; 
		  httpRequest = RestAssured 
				  		.given() 
				  		.header("Authorization","Bearer "+TokenVal); 
		  response = httpRequest.get(); System.out.println("Step 8 - Get Impact");
		  System.out.println("*******************"); 
		  res = response.getBody().asString(); 
		  jnode = mapper.readTree(res);
		  System.out.println("Actual Impact is: "+res);
		  System.out.println("Status for Get Route is: "+response.getStatusCode()+"\n"); 
		  assertEquals("compare status code",200,response.getStatusCode());
		  
		  //API for Verify Validation RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges/"+ECID+"/validation"; 
		  httpRequest = RestAssured 
				  		.given() 
				  		.header("Authorization","Bearer "+TokenVal); 
		  response = httpRequest.get(); System.out.println("Step 9 - Get Validation");
		  System.out.println("***********************"); 
		  res = response.getBody().asString(); jnode = mapper.readTree(res);
		  System.out.println("Actual Validation is: "+res);
		  System.out.println("Status for Get Route is: "+response.getStatusCode()+"\n");
		  assertEquals("compare status code",200,response.getStatusCode());
		  
		 /*
		  //API for Storing Dependency ID RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges/"+ECID+"/dependencies"; 
		  httpRequest = RestAssured 
				  		.given() 
				  		.header("Authorization","Bearer "+TokenVal); 
		  response = httpRequest.get(); System.out.println("Step 10 - Get Dependency ID");
		  System.out.println("***************************"); 
		  res = response.getBody().asString(); 
		  jnode = mapper.readTree(res); 
		  String depId = jnode.findValue("objectId").asText();
		  System.out.println("DependencyID Value is: "+depId);
		  System.out.println("Status for Get Route is: "+response.getStatusCode()+"\n");
		  assertEquals("compare status code",200,response.getStatusCode());
		  
		  //API for Dependency RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI = "http://fc8ecmtv/api/engineeringchanges/"+ECID+"/dependencies/"+depId;
		  httpRequest = RestAssured 
				  		.given()
				  		.header("Authorization","Bearer "+TokenVal)
				  		.body("{\"name\":\"test\",\"description\":\"test\"}"); 
		  response = httpRequest.post();
		  System.out.println("Step 11 - Add EC Dependency values");
		  System.out.println("**********************************");
		  System.out.println("Status for Add Desc is: "+response.getStatusCode()+"\n");
		  assertEquals("compare status code",200,response.getStatusCode());
		  
		  //API for Submit EC RestAssured.urlEncodingEnabled= false;
		  RestAssured.baseURI =
		  "http://fc8ecmtv/api/engineeringchanges/"+ECID+"/submit"; 
		  httpRequest = RestAssured 
				  		.given() 
				  		.header("Authorization","Bearer "+TokenVal)
				  		.body("{\"neededBy\":\"2020-07-08T18:30:00.000Z\"}"); 
		  response = httpRequest.post(); System.out.println("Step 12 - Submit EC");
		  System.out.println("*******************");
		  System.out.println("Status for Add Desc is "+response.getStatusCode()+"\n");
		  assertEquals("compare status code",200,response.getStatusCode());
		  */
		 

	}

}
