package getDataFromExcel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import driverPack.DriverClass;

public class GetTestData {

	static String getColValue;
	//Get this data  from properties file and update here
	static String testDatailName = "ECM_TestData.xlsx";

	public static String GetRelativeTestData(String ColumnName) throws IOException {
		String ProjectPath = System.getProperty("user.dir");
		String TestDataFilePath = ProjectPath+File.separator+"TestData\\"+testDatailName;
		FileInputStream TestdataFilePath = new FileInputStream (TestDataFilePath);

		XSSFWorkbook workbook = new XSSFWorkbook(TestdataFilePath);   
		XSSFSheet sheet = workbook.getSheet(DriverClass.ScenarioName);    

		XSSFRow row = sheet.getRow(1);
		int colCount = row.getLastCellNum();
		int rowCount = sheet.getLastRowNum();

		for(int l=2;l<=rowCount;l++){

			Row getTCIDSheet=sheet.getRow(l); //returns the logical row  
			Cell getTCIDCell=getTCIDSheet.getCell(0); //getting the cell representing the given column  
			Cell DataID = getTCIDSheet.getCell(1);
			String getTCID=getTCIDCell.getStringCellValue(); 
			String TestData_ID = DataID.getStringCellValue();

			if (getTCID.equalsIgnoreCase(DriverClass.TCID) && TestData_ID.equalsIgnoreCase(DriverClass.TestDataID) && DriverClass.TestDataID != "" ) {

				for (int m=2;m<=colCount;m++) {
					Row TcID=sheet.getRow(1); 
					Cell dataColumn=TcID.getCell(m);  
					String ColumnHeader = dataColumn.getStringCellValue();

					if (ColumnHeader.equalsIgnoreCase(ColumnName) ) {

						Row TctestCaseID=sheet.getRow(l); 
						Cell getdataColumn=TctestCaseID.getCell(m);
						String testData = getdataColumn.getStringCellValue();
						getColValue = testData;
						break;
					}

				}

			}

			workbook.close();
		}		
		return getColValue;	
	}

}  