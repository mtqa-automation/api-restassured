package getDataFromExcel;

import java.io.IOException;

import org.testng.annotations.Test;

import DemoTestAssured.DemoAPI.EcmAPImethods;
import DemoTestAssured.DemoAPI.OtherSupportingMethod;

@Test
public class GetTestScript {

	static String TestScriptStatus;

	public static String ExecuteMethods(String Method) throws Exception, IOException {

		switch (Method) {

		case "Start_Execution":
			TestScriptStatus = "True";
			OtherSupportingMethod.Start_Execution();
			break;

		case "login_ECM":
			TestScriptStatus = "True";
			String username = GetTestData.GetRelativeTestData("UserName");
			String password = GetTestData.GetRelativeTestData("Password");
			TestScriptStatus = EcmAPImethods.login_ECM(username, password);
			break;
		case "create_Scope":
			TestScriptStatus = "True";
			String targetValue = GetTestData.GetRelativeTestData("TargetValue");
			String MainPD = GetTestData.GetRelativeTestData("MainPD");
			TestScriptStatus = EcmAPImethods.create_Scope(targetValue, MainPD);
			break;

		}
		return TestScriptStatus;	
	}
}
