package DemoTestAssured.DemoAPI;

import static org.junit.Assert.assertEquals;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class EcmAPImethods {

	static String Hostname = "fc8ecmtv";

	static RequestSpecification httpRequest;
	static  Response response;
	static String res;
	static ObjectMapper mapper;
	static JsonNode jnode;

	static int StepCount = 1;
	static String TokenVal = null;
	static String ScopeID = null;
	static String ECID;
	static String EcValue;
	static String Revision;
	static String MethodStatus;


	public static String login_ECM(String userName,String Password ) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			//API for Login ECM with user name and password
			RestAssured.urlEncodingEnabled = false;
			RestAssured.baseURI = "http://"+Hostname+"/api/auth/login";
			httpRequest = RestAssured.given().auth().preemptive().basic(userName, Password)
					// .contentType("application/json").log().all();
					.contentType("application/json");
			response = httpRequest.post();
			System.out.println("Step " +StepCount+++ ": Login ECM APP");
			System.out.println("************************");
			TokenVal = response.header("Authorization");
			System.out.println("Token Value is: " + TokenVal + "\n");
			assertEquals("compare logged username", userName, response.path("username"));
			assertEquals("compare status code", 200, response.getStatusCode());
		}

		catch (Exception e){
			//System.out.println("Exception for login_ECM "+e);
			MethodStatus = "False";
		}

		return MethodStatus;

	}

	public static String create_Scope(String TargetVal, String MainPD) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try { 
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/scopes";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"actions\":[{\"target\":[\""+TargetVal+"\"],\"operation\":\"add\",\"data\":\""+MainPD+"\"}]}");
			response =httpRequest.post();
			System.out.println("Step " +StepCount+++ ": Create Scope");
			System.out.println("************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res); 
			ScopeID = jnode.findValue("objectId").asText();
			System.out.println("ScopeID Value is: "+ScopeID+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch (Exception e) {
			//System.out.println("Exception for create_Scope "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String create_EC() throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges";
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"ecType\":\"ROUTE\",\"scopeId\":\""+ScopeID+"\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++ ": Create EC");
			System.out.println("******************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res);
			ECID = jnode.findValue("objectId").asText();
			System.out.println("ECID is: "+ECID);
			EcValue = jnode.findValue("ecId").asText();
			System.out.println("EC Value is: "+EcValue+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch (Exception e) {
			//System.out.println("Exception for create_EC "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String add_EC_Desc(String ECDesc) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {  
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/engineeringchanges/"+ECID;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"actions\":{\"operation\":\"editDescription\",\"data\":[\""+ECDesc+"\"]}}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++": Add EC Description");
			System.out.println("***************************");
			System.out.println("Status for Add Desc is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for add_EC_Desc "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}


	public static String getRouteDetails(String MainPD) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {  
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/routes/"+MainPD+"/" + ECID;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal);    		  			
			response =httpRequest.get();
			System.out.println("Step "+StepCount+++": Get Route Details");
			System.out.println("**************************");
			res = response.getBody().asString(); 
			mapper = new ObjectMapper();
			jnode = mapper.readTree(res);
			Revision = jnode.findValue("revision").asText();
			System.out.println("Revision Value is: "+Revision);
			System.out.println("Status for Get Route is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch (Exception e) {
			//System.out.println("Exception for getRouteDetails "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String updateCarrierCategory(String MainPD,String OperationNo, String protocolzone ) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/mainpds/"+MainPD+"/"+ECID+"/"+Revision+"/operations/"+OperationNo;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"protocolZone\":\""+protocolzone+"\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++": Update Carrier Category");
			System.out.println("********************************");
			System.out.println("Status for update carrier category is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for updateCarrierCategory "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}

	public static String getLRA(String MainPD,String OperationNo) throws JsonMappingException, JsonProcessingException {
		MethodStatus = "True";
		try {
			RestAssured.urlEncodingEnabled= false; 
			RestAssured.baseURI  =  "http://"+Hostname+"/api/mainpds/"+MainPD+"/"+ECID+"/"+Revision+"/operations/"+OperationNo;
			httpRequest = RestAssured
					.given()
					.header("Authorization","Bearer "+TokenVal)
					.body("{\"protocolZone\":\"BEOL\"}");
			response =httpRequest.post();
			System.out.println("Step "+StepCount+++": Update Carrier Category");
			System.out.println("********************************");
			System.out.println("Status for update carrier category is: "+response.getStatusCode()+"\n");
			assertEquals("compare status code",200,response.getStatusCode());
		}
		catch(Exception e) {
			//System.out.println("Exception for getLRA "+e);
			MethodStatus = "False";
		}
		return MethodStatus;
	}


}